#!/bin/sh

set -e

cd src

yacc -d parse.y
mv y.tab.h parse.h
mv y.tab.c parse.c
echo '#define YYSTYPE int\nextern YYSTYPE yylval;' >> parse.h

/bin/sh ./mkskel.sh . freebsd-m4 2.6.4 > skel.c

FILES="buf ccl dfa ecs filter gen libmain main misc nfa options regex scanflags scanopt skel sym tables tables_shared tblcmp parse yylex"

OBJS=""
for f in $FILES; do
    echo cc -DHAVE_CONFIG_H -I. -DLOCALEDIR=\"/usr/local/share/locale\" -g -O2 -c -o flex-$f.o $f.c
    cc -DHAVE_CONFIG_H -I. -DLOCALEDIR=\"/usr/local/share/locale\" -g -O2 -c -o flex-$f.o $f.c
    OBJS="$OBJS flex-$f.o"
done

echo cc -g -O2 -o flex $OBJS -lm
cc -g -O2 -o flex $OBJS -lm
